package com.example.demo;

import static io.restassured.RestAssured.get;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@LocalServerPort
	private int port;

	@Test
	public void contextLoads() {}

	@Test
	public void withoutSearch() {

		List<Object> list = get("http://localhost:" + port + "/tuples").body().jsonPath().getList("$");
		assertEquals(3, list.size());
	}

	@Test
	public void simpleSearch() {

		List<Object> list = get("http://localhost:" + port + "/tuples?search=name:*th*").body().jsonPath().getList("$");
		assertEquals(3, list.size());
		list = get("http://localhost:" + port + "/tuples?search=name:*wit*").body().jsonPath().getList("$");
		assertEquals(2, list.size());
	}

	@Test
	public void commaSearch() {

		List<Object> list = get("http://localhost:" + port + "/tuples?search=name:*,*").body().jsonPath().getList("$");
		assertEquals(1, list.size());
	}

	@Test
	public void spaceSearch() {

		// White space gets transformed to plus sign as the request is sent
		List<Object> list = get("http://localhost:" + port + "/tuples?search=name:*+*").body().jsonPath().getList("$");
		assertEquals(1, list.size());
	}

	@Test
	public void justToSeeWhatHappens() {

		List<Object> list = get("http://localhost:" + port + "/tuples?search=name:* *").body().jsonPath().getList("$");
		assertEquals(1, list.size());
	}
}
