package com.example.demo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.specification.DefaultSpecificationsBuilder;
import com.example.demo.specification.SearchOperation;

@RepositoryRestController()
public class TupleController {

	@Autowired
	private TupleRepository tupleRepository;

	/**
	 * Replacement for {@link com.google.common.base.Joiner Joiner}.
	 * @param on {@link java.lang.String String} separator
	 * @param array {@link java.lang.String String}s to be joined
	 * @return joined {@link java.lang.String String}s
	 */
	public static String join(String on, String array[]) {

		StringBuilder builder = new StringBuilder();
		for (String string : array) {
			builder.append(string).append(on);
		}
		builder.setLength(builder.length() - on.length());
		return builder.toString();
	}

	protected Specification<Tuple> resolveSpecification(String searchParameters) {

		DefaultSpecificationsBuilder<Tuple> builder = new DefaultSpecificationsBuilder<Tuple>();
		// String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
		String operationSetExper = join("|", SearchOperation.SIMPLE_OPERATION_SET);
		Pattern pattern = Pattern.compile(
		  "(\\p{Punct}?)(\\w+?)("
		  + operationSetExper
		  + ")(\\p{Punct}?)(\\p{L}+?)(\\p{Punct}?),");
		Matcher matcher = pattern.matcher(searchParameters + ",");

		while (matcher.find()) {
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3),
			matcher.group(5), matcher.group(4), matcher.group(6));
		}

		return builder.build();
	}

    @GetMapping("tuples")
	public ResponseEntity<Object> findAll(@RequestParam(value = "search", required = false) String search) {

    	try {

    		Specification<Tuple> spec = resolveSpecification(search);
    		return ResponseEntity.ok(tupleRepository.findAll(spec));

    	} catch (Throwable t) {

    		t.printStackTrace();
    		return ResponseEntity.badRequest().build();
    	}
    }
}
