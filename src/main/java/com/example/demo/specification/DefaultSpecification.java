package com.example.demo.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class DefaultSpecification <T> implements Specification<T> {

	private static final long serialVersionUID = 8101008713432735355L;

	private SpecSearchCriteria criteria;

	public DefaultSpecification(final SpecSearchCriteria criteria) {
		this.criteria = criteria;
	}

	public SpecSearchCriteria getCriteria() {
		return criteria;
	}

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		switch (criteria.getOperation()) {
		case EQUALITY:
			return builder.equal(root.get(criteria.getKey()), criteria.getValue());
		case NEGATION:
			return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
		case GREATER_THAN:
			return builder.greaterThan(builder.lower(root.<String> get(criteria.getKey())), criteria.getValue().toString().toLowerCase());
		case LESS_THAN:
			return builder.lessThan(builder.lower(root.<String> get(criteria.getKey())), criteria.getValue().toString().toLowerCase());
		case LIKE:
			return builder.like(builder.lower(root.<String> get(criteria.getKey())), criteria.getValue().toString().toLowerCase());
		case STARTS_WITH:
			return builder.like(builder.lower(root.<String> get(criteria.getKey())), (criteria.getValue() + "%").toLowerCase());
		case ENDS_WITH:
			return builder.like(builder.lower(root.<String> get(criteria.getKey())), ("%" + criteria.getValue()).toLowerCase());
		case CONTAINS:
			return builder.like(builder.lower(root.<String> get(criteria.getKey())), ("%" + criteria.getValue() + "%").toLowerCase());
		case COLLECTION_CONTAINS:
			return builder.isMember(criteria.getValue(), root.get(criteria.getKey()));
		case PROPERTY_NEGATION:
			String fieldArray[] = criteria.getKey().split("\\.");
			return builder.notEqual(root.get(fieldArray[0]).get(fieldArray[1]), criteria.getValue());
		default:
			return null;
		}
	}
}
